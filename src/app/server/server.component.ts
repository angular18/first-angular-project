import { Component } from '@angular/core';

@Component({
  selector : 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css']
})

export class ServerComponent {
  btnDisabled = false;
  serverStatus = "";
  data = "";
  flag = 0
  serverCreated = false
  captureData = (e)=> {
    this.data = e.target.value;
  }
  getColor = () =>{
    return `rgb(${Math.round(Math.random()*255)},${Math.round(Math.random()*255)},${Math.round(Math.random()*255)})`
  }
  servers = []
  startServer = (e)=>{
    this.flag = Math.random()*10;
    this.servers.push(this.data)
    this.serverCreated = true;
    this.data = "";
  }
  constructor() {
      setTimeout(()=> {
        this.btnDisabled = true;
      }, 2000)
  }
}